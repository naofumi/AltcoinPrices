package eu.uwot.fabio.altcoinprices;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;


public class SettingsActivity extends AppCompatActivity {

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Switch currencySwitch;
    String currency;
    EditText periodInput;
    String period;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        /* load  user settings */
        prefs = getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode
        editor = prefs.edit();
        this.currency = prefs.getString("currency", "EUR");
        this.period = prefs.getString("period", "30");
        editor.commit();
        /* end load  user settings */

        /* currency switch */
        currencySwitch = (Switch) findViewById(R.id.currency_switch);

        currencySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    currencySwitch.setChecked(true);
                    editor.putString("currency", "USD");
                    editor.commit();
                } else {
                    currencySwitch.setChecked(false);
                    editor.putString("currency", "EUR");
                    editor.commit();
                }
            }
        });

        if (this.currency.equals("EUR")) {
            currencySwitch.setChecked(false);
        } else {
            currencySwitch.setChecked(true);
        }
        /* end currency switch */

        /* set candlestick period */
        periodInput = (EditText)findViewById(R.id.periodInput);
        periodInput.setText(this.period);

        periodInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // empty
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // empty
            }

            @Override
            public void afterTextChanged(Editable s) {
                period = periodInput.getText().toString();
                if (period.equals("")) {
                    editor.putString("period", "30");
                    editor.commit();
                } else {
                    editor.putString("period", period);
                    editor.commit();
                }
                //Log.d("DEBUG", "period: " + period);
            }
        });
        /* end set candlestick period */

        /* about text - makes links clicable */
        TextView aboutText = (TextView) findViewById(R.id.about);
        aboutText.setMovementMethod(LinkMovementMethod.getInstance());
        /* end about text */
    }
}
